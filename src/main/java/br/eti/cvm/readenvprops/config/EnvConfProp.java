package br.eti.cvm.readenvprops.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties("myapp")
@Getter
@Setter
public class EnvConfProp {
	private String dbHost;
	private Integer dbPort;
	private String dbName;
	private String dbUser;
	private String dbPass;
}
