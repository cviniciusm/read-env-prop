package br.eti.cvm.readenvprops;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import br.eti.cvm.readenvprops.config.EnvConfProp;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class MyCommandLineRunner implements CommandLineRunner {

	private final EnvConfProp envConfProp;

	@Value("${spring.datasource.url}")
	private String datasourceUrl;

	@Value("${spring.datasource.username}")
	private String username;

	@Value("${spring.datasource.password}")
	private String password;

	@Override
	public void run(String... args) throws Exception {
		System.out.println("DB_HOST = " + envConfProp.getDbHost());
		System.out.println("DB_PORT = " + envConfProp.getDbPort());
		System.out.println("DB_NAME = " + envConfProp.getDbName());
		System.out.println("DB_USER = " + envConfProp.getDbUser());
		System.out.println("DB_PASS = " + envConfProp.getDbPass());

		System.out.println();

		System.out.println("spring.datasource.url=" + datasourceUrl);
		System.out.println("spring.datasource.username=" + username);
		System.out.println("spring.datasource.password=" + password);
	}

}
