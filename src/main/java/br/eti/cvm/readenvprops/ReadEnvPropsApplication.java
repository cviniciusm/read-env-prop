package br.eti.cvm.readenvprops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReadEnvPropsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReadEnvPropsApplication.class, args);
	}

}
