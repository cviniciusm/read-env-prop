# read-env-prop

### Usage
```
C:\desenvolvimento\git\read-env-props>java -jar target\read-env-props-0.0.1-SNAPSHOT.jar

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::             (v2.2.0.M5)

2019-08-14 21:04:40.464  INFO 24084 --- [           main] b.e.c.r.ReadEnvPropsApplication          : Starting ReadEnvPropsApplication v0.0.1-SNAPSHOT on LAPTOP-56J1L2VC with PID 24084 (C:\desenvolvimento\git\read-env-props\target\read-env-props-0.0.1-SNAPSHOT.jar started by cassius in C:\desenvolvimento\git\read-env-props)
2019-08-14 21:04:40.468  INFO 24084 --- [           main] b.e.c.r.ReadEnvPropsApplication          : No active profile set, falling back to default profiles: default
2019-08-14 21:04:41.109  INFO 24084 --- [           main] b.e.c.r.ReadEnvPropsApplication          : Started ReadEnvPropsApplication in 1.211 seconds (JVM running for 2.03)
DB_HOST = db.example.org
DB_PORT = 3306
DB_NAME = mydatabase
DB_USER = myappusername
DB_PASS = myapppassword

spring.datasource.url=jdbc:mysql://db.example.org:3306/mydatabase
spring.datasource.username=myappusername
spring.datasource.password=myapppassword

C:\desenvolvimento\git\read-env-props>set MYAPP_DB_HOST=another.example.com

C:\desenvolvimento\git\read-env-props>set MYAPP_DB_PORT=5121

C:\desenvolvimento\git\read-env-props>set MYAPP_DB_NAME=anotherdatabase

C:\desenvolvimento\git\read-env-props>set MYAPP_DB_USER=userdb

C:\desenvolvimento\git\read-env-props>set MYAPP_DB_PASS=P@ssw0rd

C:\desenvolvimento\git\read-env-props>java -jar target\read-env-props-0.0.1-SNAPSHOT.jar

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::             (v2.2.0.M5)

2019-08-14 21:06:40.629  INFO 10692 --- [           main] b.e.c.r.ReadEnvPropsApplication          : Starting ReadEnvPropsApplication v0.0.1-SNAPSHOT on LAPTOP-56J1L2VC with PID 10692 (C:\desenvolvimento\git\read-env-props\target\read-env-props-0.0.1-SNAPSHOT.jar started by cassius in C:\desenvolvimento\git\read-env-props)
2019-08-14 21:06:40.633  INFO 10692 --- [           main] b.e.c.r.ReadEnvPropsApplication          : No active profile set, falling back to default profiles: default
2019-08-14 21:06:41.259  INFO 10692 --- [           main] b.e.c.r.ReadEnvPropsApplication          : Started ReadEnvPropsApplication in 1.219 seconds (JVM running for 1.997)
DB_HOST = another.example.com
DB_PORT = 5121
DB_NAME = anotherdatabase
DB_USER = userdb
DB_PASS = P@ssw0rd

spring.datasource.url=jdbc:mysql://another.example.com:5121/anotherdatabase
spring.datasource.username=userdb
spring.datasource.password=P@ssw0rd
```